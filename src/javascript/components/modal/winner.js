import { showModal } from './modal'
import App from '../../app';
import { createFighterImage } from '../fighterPreview'
export function showWinnerModal(fighter) {
  showModal({
    title: `${fighter.name} is a winner!`,
    bodyElement: createFighterImage(fighter),
    onClose: () => {
      let root = document.getElementById('root');
      root.innerHTML = '';
      new App();
    }
  })
}